<?php
if(@$_POST['id']!=null && @$_POST['nama']!=null){
	$id = @$_POST['id'];
	$nama = @$_POST['nama'];
?>	
<div class="section container">
	<h5 class="blue-text">Ubah Data Supplier</h2>
		<table>
			<form class="col s12">
				<div class="row"> 
					<div class="input-field col s5">
						<input value="<?php echo $id; ?>" type="text" id="id" class="validate" disabled>
						<label class="active pink-text">Id</label>
					</div>
				</div>
				<div class="row"> 
					<div class="input-field col s5">
						<input value="<?php echo $nama; ?>" type="text" id="nama" placeholder="nama supplier" required >
						<label class="active pink-text">Nama</label>
					</div>
				</div>
				<div class="row">
					<div class="input-filed col s3">
						<button class="btn waves-effect waves-light orange" type="reset" id="batal">Batal
						</button>
					</div>
					<div class="input-filed col s2">
						<button class="btn waves-effect waves-light"id="simpanedit_supplier">Simpan
						</button>
					</div>
				</div>
			</form>
		</table>
</div>
<?php
}else{
?>
<div class="section container">
<h5 class="blue-text">Tambah Data Supplier</h2>
<table>
	<form class="col s12">
		<div class="row"> 
			<div class="input-field col s5">
				<input type="text" id="id" placeholder="id suppliers"/>
				<label class="pink-text">Id</label>
			</div>
		</div>
		<div class="row"> 
			<div class="input-field col s5">
				<input type="text" id="nama" placeholder="nama supplier"/>
				<label class="pink-text">Nama</label>
			</div>
		</div>
		<div class="row">
			<div class="input-filed col s3">
				<button class="btn waves-effect waves-light orange" type="reset" id="batal">Batal
					<i class="mdi-content-send right"></i>
				</button>
			</div>
			<div class="input-filed col s2">
				<button class="btn waves-effect waves-light" type="submit" id="simpantambah_supplier">Simpan
					<i class="mdi-content-send right"></i>
				</button>
			</div>
		</div>
	</form>
</table>
</div>
<?php } ?>
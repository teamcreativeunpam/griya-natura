
	<nav class="lighten-1" role="navigation">
		<div class="nav-wrapper container"><a id="logo-container" href="./" class="brand-logo">HALAMAN ########</a>
			<ul class="right hide-on-med-and-down">
				<li><a class="waves-effect waves-light" href="./">Back Home</a></li>
				<li><a id="supplier" class="waves-effect waves-light">Data Suplier</a></li>
				<li><a id="medicine" class="waves-effect waves-light">Data Medicine</a></li>
				<li><a id="dokter" class="waves-effect waves-light">Data Dokter</a></li>
			</ul>
			<ul id="nav-mobile" class="side-nav">
				<li><a href="./">Back Home</a></li>
				<li><a href="">Data Supplier</a></li>
				<li><a href="">Data Medicine</a></li>
			</ul>
			<a href="#" data-activates="nav-mobile" class="button-collapse"><i class="mdi-navigation-menu"></i></a>
		</div>
	</nav>